"use strict"

function showBoard(svg)  {
    document.getElementById("boardDiv").innerHTML = svg;
}

function showBoardResponse(response)  {
    response.text().then(showBoard);
}

function loadBoard()  {
    window.socketConn = launchSocket();
    fetch("board").then(showBoardResponse);
}


function postMove(x, y)  {
    var s = x + " " + y;
    if (window.socketConn.readyState <= 1)  {
        console.log(s);
        window.socketConn.send(s);
    }  else  {
        console.log("crap");
    }
}


function getId()  {
    var splitHref = window.location.href.split("/");
    console.log(splitHref[splitHref.length - 2]);
    return splitHref[splitHref.length - 2];
}

function launchSocket()  {
    var ws = new WebSocket("ws://" + window.location.host);
    
    ws.onopen = () =>  {
        ws.send(getId())
        console.log("yay: " + getId());
    };
  
    ws.onmessage = evt =>  {
        var m = JSON.parse(evt.data);
        console.log(m);
        document.getElementById("gameMetadata").outerHTML = m.metadata;
        placed = document.getElementById("placed");
        if(m.contents.tag == "Stone")  {
            placed.innerHTML += m.contents.contents;
            document.getElementById("options").setAttribute("class", "wrap" + m.currMove);
        }  else if(m.contents.tag == "Stones")  {
            document.getElementById("unique").outerHTML = m.contents.contents;
        }
    };
  
    ws.onclose = function()  {
        console.log("ws closed");
        window.socketConn = launchSocket(true);
    };
    
    ws.onbeforeunload = evt =>  {
        console.log("we're closing");
        socket.close();
    };
    return ws;
}


function initSlider()  {
    var slider        = document.getElementById("handicap"     );
    var stoneHandicap = document.getElementById("stoneHandicap");
    var komi          = document.getElementById("komi"         );
    console.log("oninput___");
    slider.oninput = function()  {
        console.log("oninput");
        handicap = parseInt(slider.value)
        if (handicap == 0)  {
            komi.innerHTML = "komi: .5";
        } else {
            komi.innerHTML = "komi 6.5"
        }
        komi.innerHTML = "komi " + (handicap == 0? ".5": "6.5")

        stoneHandicap.innerHTML = (handicap <= 1? "no": slider.value) + " stones"
    }
    slider.oninput();
}
