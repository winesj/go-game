import GoBoard
import GoSVG
import qualified GoSVGBuilder as S
import Data.Char
import Diagrams.Prelude
import Diagrams.Backend.SVG
import Diagrams.Size
import Data.Maybe

toGoBoard :: String -> Maybe Board
toGoBoard = makeMoves . map (toTuple . toIntList) . lines
    where
        toIntList = map (\x -> ord x - ord 'a')

toTuple :: [a] -> (a, a)
toTuple (x:xx:xs) = (x, xx)


board = toGoBoard <$> readFile "testGame.txt"

--svg :: IO (Maybe (Diagram B))
-- svg = (fmap . fmap $ toStones) board

outputIfJust Nothing = putStrLn "bad move in move list"
outputIfJust (Just svg) = S.out svg
-- outputIfJust (Just svg) = renderSVG "output.html" (dims . r2 $ (800.0, 800.0)) svg


main :: IO ()
main = do
    b <- board
    print . fromMaybe GoBoard.empty $ b
    outputIfJust b



