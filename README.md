[![builds.sr.ht status](https://builds.sr.ht/~jackwines/go-game.svg)](https://builds.sr.ht/~jackwines/go-game?)
This takes a game of go, represented as a series of moves, applies the rules for each move, and then outputs them as `svg` using `blaze-svg`. It then uses `scotty` and makes a fully-functioning website that lets you play a game of go with anyone anywhere. Using `websockets`, moves your opponent makes update in real time.

These already exist, but they require logins and whatnot. This one doesn't.

To run:
* make sure you have [stack](https://docs.haskellstack.org/en/stable/install_and_upgrade/) installed.
* Run the following (this will take a while).
```
stack build +RTS -I0
stack exec-GoGame-exe
```
* head to [http://localhost:3000/](http://localhost:3000/)
