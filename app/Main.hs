import Control.Monad.IO.Class
import Control.Monad
import Database
import Data.Acid
import Data.Acid.Advanced
import qualified Board as GB
import Data.Maybe
import qualified Data.Text as T
import Data.Text.Lazy.Encoding
import qualified Data.Text.Lazy as TL
import qualified Data.ByteString.Lazy as BSL
import Web.Scotty as Sc
import qualified Network.Wai.Middleware.Gzip as Sc
import qualified Network.Wai.Handler.WebSockets as WaiWs
import qualified Network.WebSockets as WS
import qualified Network.Wai as Wai
import qualified Network.Wai.Handler.Warp as Warp
import Control.Exception.Base
import Control.Concurrent
import SVGBuilder as SVG
import Data.Aeson
import qualified Data.Sequence as S
import Network.Wai.Middleware.Static as S

-- Turns a bytestring into text, then into two ints, split by a space.
readOutput :: BSL.ByteString -> (Int, Int)
readOutput = toTuple . map (read . T.unpack) . T.split (== ' ') . TL.toStrict . decodeUtf8

toTuple (x:xs:_) = (x, xs)

instance ToJSON Render where
    toEncoding = genericToEncoding defaultOptions

instance ToJSON RenderContents where
    toEncoding = genericToEncoding defaultOptions

-- It's both a scotty app and a websockets app
main :: IO ()
main = do
    putStrLn "http://localhost:3000"
    let port = 3000
    let settings = Warp.setPort port Warp.defaultSettings
    state <- openLocalDB
    numBoards <- query' state QueryNumSeq
    connections <- newMVar . S.replicate numBoards $ ([] :: [(Int, WS.Connection)])
    sapp <- sApp state connections
    let wsapp = wsApp state connections
    Warp.runSettings settings
        . S.static $ WaiWs.websocketsOr WS.defaultConnectionOptions wsapp sapp

-- The scotty app, serves the static files, handles the initial get of the board,
-- and lets you change the board by posting a stone, though websockets is the
-- preferred method for that.
sApp :: AcidState DB -> MVar (S.Seq [(Int, WS.Connection)]) -> IO Wai.Application
sApp state connections = scottyApp $ do
    Sc.middleware $ Sc.gzip $ Sc.def { Sc.gzipFiles = Sc.GzipCompress }
    get "/:id/game"      $ file "static/board.html"
    get "/"              $ file "static/index.html"
    get "/:id/flipbookjson" $ do
        boardId <- param "id"
        board <- query' state (QueryBoard boardId)
        liftIO $ putStrLn "pls"
        Sc.json . makeFlipbook $ board
    get "/:id/board" $ do
        boardId <- param "id"
        board <- query' state (QueryBoard boardId)
        text . fullHtml $ board
    post "/poststone/:id" $ do
        boardId <- param "id"
        textBoard <- addStone boardId state connections =<< body
        text textBoard
    post "/newGame/" $ do
        size     <- param "size"
        handicap <- param "handicap"
        black    <- param "black"
        white    <- param "white"
        let opts = GB.Options {
            GB.black    = if TL.null black then Nothing else Just black,
            GB.white    = if TL.null white then Nothing else Just white,
            GB.size     = size ,
            GB.handicap = handicap
                              }
        liftIO $ update state (AppendBoard opts)
        newId <- query' state QueryNumSeq
        liftIO $ modifyMVar_ connections (\x -> return $ x S.|> [])
        redirect $ (TL.pack . show . pred $ newId) `TL.append` "/game"

-- Since both webSockets and scotty need to place a stone on the board, it's
-- abstracted away.
addStone :: MonadIO m => Int -> AcidState DB -> MVar (S.Seq [(Int, WS.Connection)]) -> BSL.ByteString -> m (TL.Text)
addStone boardId state connections input = do
    let loc = readOutput input
    b <- query' state (QueryBoard boardId)
    let (newBoard, eaten) = fromJust . GB.makeMove loc $ b
    update' state (UpdateBoard boardId newBoard)
    cns <- liftIO $ fmap (flip S.index $ boardId) . readMVar $ connections
    liftIO . mapM (handleConn newBoard) $ cns
    let textB = fullHtml newBoard
    return textB
    where
        handleConn board (_, conn)= WS.sendTextData conn . encode . SVG.render $ board

-- Accepts the connection, adds it to the connections MVar, and pings.
-- It also listens for messages, which indicate stone placements.
-- When it recieves one, it responds with an updated SVG.
wsApp :: AcidState DB -> MVar (S.Seq [(Int, WS.Connection)]) -> WS.ServerApp
wsApp state connections pending = do
    putStr "ws connected"
    conn <- WS.acceptRequest pending
    whatIRead <- T.unpack <$> WS.receiveData conn
    putStrLn whatIRead
    let boardId = read whatIRead
    connId <- newConnId boardId
    liftIO $ modifyConnectionSeq ((connId, conn) :) boardId
    WS.forkPingThread conn 25
    finally (broadcast boardId conn) (onclose boardId connId)
    where
        broadcast boardId conn = forever (addStone boardId state connections =<< WS.receiveData conn)

        onclose boardId connId = modifyConnectionSeq (filter ((==) connId . fst)) boardId

        modifyConnectionSeq f i = modifyMVar_ connections (return . S.adjust f i)

        newConnId boardId = length . (flip S.index) boardId <$> readMVar connections
