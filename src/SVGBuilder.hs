module SVGBuilder where

import qualified Board as GB
import Graphics.Svg
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Data.Array as A
import Data.List
import qualified Text.Blaze.Html5 as B
import qualified Text.Blaze.Html5.Attributes as BA
import Text.Blaze.Html.Renderer.Text
import GHC.Generics
import Data.Maybe

page_ :: Element -> Element
page_ = makeElement "page"

html_ = makeElement "html"

makeFlipbook :: GB.Board -> [TL.Text]
makeFlipbook b = map (renderText . page) . reverse . tails . GB.moves $ b
    where
        page s = board $ b {GB.moves = s}

-- SVG definitions, save us space in the final text.
defs = defs_ [] (gridPattern <> stone)

-- Given a board, give back a SVG depicting that board.
board :: GB.Board -> Element
board b = svg size $ defs <> rect <> gridTranslation (grid (pred size) <> (starPoints b) <> (combineStones . stones $ b))
    where
        size :: Int
        size = GB.size . GB.options $ b


starPoints :: GB.Board -> Element
starPoints = g_ [Id_ <<- "starPoints"] . mconcat . map starPointAt . GB.cleanStarPoints . GB.size . GB.options

starPointAt (x, y) = circle_ [R_ <<- "0.15px", Fill_ <<- "black", Cx_ <<- conv x, Cy_ <<- conv y]

-- A 19*19 viewBox makes things much easier
svg :: Int -> Element -> Element
svg size = svg_ [ViewBox_ <<- "0 0 " `T.append` sz `T.append` " " `T.append` sz, Id_ <<- "stones"]
    where
        sz = conv size

gridTranslation :: Element -> Element
gridTranslation = g_ [Id_ <<- "stoneGrid", Transform_ <<- "translate(0.5, 0.5)"]

stoneSVG :: GB.Board -> Element
stoneSVG b = withDefCol (head . GB.moves $ b, Just . GB.stoneNot . GB.currMove $ b)

tupleMap :: (a -> b) -> (a, a) -> (b, b)
tupleMap f (a, b) = (f a, f b)

combineStones = g_ [Id_ <<- "unique"] . uncurry (<>)

stones :: GB.Board -> (Element, Element)
stones b =  combine . tupleMap makeStones . partition (isJust . snd) . A.assocs . GB.stones $ b
    where

        makeStones :: [((Int, Int), Maybe GB.Stone)] -> Element
        makeStones = mconcat . map withDefCol

        combine :: (Element, Element) -> (Element, Element)
        combine (placed, options) = (g_ [Id_ <<- "options"
                                      , Class_ <<- "wrap" `T.append` (colorStr . GB.currMove $ b)] options, g_ [Id_ <<- "placed"] placed)

withDefCol ::((Int, Int), Maybe GB.Stone) -> Element
withDefCol (coords, s) = (stoneCore coords) `with` attribs s
    where
        attribs (Just stone) = [Fill_ <<- colorStr stone]
        attribs Nothing      = [Class_ <<- "stoneOption",
                                Onclick_ <<-  T.pack ("postMove" ++ (show $ coords))]

stone :: Element
stone = circle_ [R_ <<- ".40", Id_ <<- "stone"]

stoneCore :: (Int, Int) -> Element
stoneCore (x, y) = use_ $ [XlinkHref_ <<- "#stone", X_ <<- conv x, Y_ <<- conv y]

colorStr :: GB.Stone -> T.Text
colorStr GB.Black = "Black"
colorStr GB.White = "White"

conv :: Show a => a -> T.Text;
conv = T.pack . show

grid :: Int -> Element
grid size = rect_ [Width_ <<- conv size, Height_ <<- conv size, Fill_ <<- "url(#grid)"]

rect :: Element
rect = rect_ [Width_ <<- "100%", Height_ <<- "100%", Fill_ <<- "#DCB35C"]

gridPattern :: Element
gridPattern = pattern_
               [Stroke_width_ <<- "0.05",
                Stroke_       <<- "Black",
                Id_           <<- "grid",
                PatternUnits_ <<- "userSpaceOnUse",
                Width_        <<- "1",
                Height_       <<- "1"]
                $ rect_ [Width_       <<- "1",
                        Height_       <<- "1",
                        Stroke_       <<- "Black",
                        Stroke_width_ <<- "0.1",
                        Fill_         <<- "none"]

fullHtml b = renderHtml $ (B.preEscapedToHtml . renderText . board $ b) <> (gameMetadata b)

gameMetadata b = (B.div metadataContents) B.! (BA.id "gameMetadata")
    where
        metadataContents  = mconcat . map makeMarkup
                            $ [("turn", (currentPlayerPicture $ b))
                              , ("komi", (TL.pack . show . GB.komi $ b))
                              , ("handicap",  (TL.pack . show . GB.cleanHandicap $ b))]
                              ++ (catMaybes
                                [(,) "black" <$> (GB.black . GB.options $ b)
                              ,  (,) "white" <$> (GB.white . GB.options $ b)])

        makeMarkup :: B.ToMarkup a => (T.Text, a) -> B.Html
        makeMarkup (name, value) = B.div $ (B.toMarkup (T.append name ": ")) <> (B.preEscapedToHtml value)


currentPlayerPicture b = renderText
                       . svg_ [ViewBox_ <<- "0 0 0.8 0.8", Id_ <<- "currPlayer"]
                       . with stone $ [Fill_ <<- (colorStr . GB.currMove $ b), Cx_ <<- "50%", Cy_ <<- "50%"]

stonesSVG = renderText . combineStones . stones

data Render = Render {
                        metadata :: TL.Text,
                        contents :: RenderContents,
                        currMove :: T.Text
                     } deriving (Generic, Show)

data RenderContents = Stone TL.Text | Stones TL.Text deriving (Generic, Show)

render b = Render {
                    metadata = renderHtml . gameMetadata $ b,
                    contents = Stones (renderText . combineStones . stones $ b),
                    currMove = colorStr . GB.currMove $ b
                  }
