module Database where

import qualified Board as GB
import Data.Acid
import Control.Monad.Reader (ask)
import Control.Monad.State (modify)
import Data.SafeCopy
import Data.Typeable
import qualified Data.Sequence as S

data DB = DB (S.Seq GB.Board) deriving (Show, Typeable)


getSeq (DB seq) = seq

$(deriveSafeCopy 0 'base ''GB.Board)
$(deriveSafeCopy 0 'base ''GB.Stone)
$(deriveSafeCopy 0 'base ''GB.Options)
$(deriveSafeCopy 0 'base ''DB)

openLocalDB = openLocalState . DB . S.singleton . GB.makeBoard $ (GB.Options {
    GB.black = Nothing,
    GB.white = Nothing,
    GB.size = 19,
    GB.handicap = 1
                                                                         })

appendBoard :: GB.Options -> Update DB ()
appendBoard opts = modify add
    where
        add (DB seq) = DB $ seq S.|> (GB.makeBoard opts)

updateBoard :: Int -> GB.Board -> Update DB ()
updateBoard index b = modify go
    where
        go (DB seq) = DB $ S.update index b seq

queryBoard :: Int -> Query DB GB.Board
queryBoard index = (flip S.index) index . getSeq <$> ask

queryNumSeq :: Query DB Int
queryNumSeq = S.length . getSeq <$> ask

makeAcidic ''DB['queryBoard, 'updateBoard, 'appendBoard, 'queryNumSeq]
