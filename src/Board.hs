module Board
    where

-- ( Board
-- , Stone
-- , empty
-- , makeMove
-- , makeMoves
-- ) where

import qualified Data.Set as Set
import qualified Data.Array as A
import Data.Maybe
import Control.Applicative
import Data.Ix
import qualified Data.Text.Lazy as TL


data Stone = Black | White deriving (Eq, Ord)
data Board = Board {
                    moves :: [Maybe (Int, Int)],
                    stones :: A.Array (Int, Int) (Maybe Stone),
                    options :: Options
                   } deriving (Eq, Ord)

data Options = Options {
                            black :: Maybe TL.Text,
                            white :: Maybe TL.Text,
                            size :: Int,
                            handicap :: Int
                           } deriving (Eq, Ord)

instance Show Stone where
    show Black = "●"
    show White = "○"

instance Show Board where
    show board = foldl1 lineConcat . map getLine . allStones $ board
        where

            getLine :: [(Int, Int)] -> String
            getLine = foldl1 (++) . map (showMaybeStone . (flip getStone) board)

komi :: Board -> Double
komi b
    | 0 == (handicap . options $ b) = 0.5
    | otherwise                  = 6.5

showMaybeStone :: Maybe Stone -> String
showMaybeStone Nothing = " "
showMaybeStone (Just s) = show s

stoneNot :: Stone -> Stone
stoneNot Black = White
stoneNot White = Black


makeBoard :: Options -> Board
makeBoard opts = Board {
                        moves = map Just komiList,
                        stones = A.array arrBounds
                            $ (zip (range arrBounds) . repeat $ Nothing)
                            ++ (zip komiList          . repeat $ Just Black),
                       options = opts
                       }
    where
        arrBounds = ((0, 0), (pred s, pred s))
        komiList = take (if h < 2 then 0 else h) . starPoints $ s
        s = size opts
        h = handicap opts

cleanHandicap :: Board -> Int
cleanHandicap b
    | (handicap . options $ b) < 2 = 0
    | otherwise                    = handicap . options $ b

emptyClone :: Board -> Board
emptyClone = makeBoard . options

permutations l = (,) <$> l <*> l

cleanStarPoints 9  = filter (\(x, y) -> (y /= 4) && (x /= 4)) . starPoints $ 9
cleanStarPoints sz = starPoints sz


starPoints :: Int -> [(Int, Int)]
starPoints 19 = [(15,3),(3,15),(15,15),(3,3),(9,9),(3,9),(15,9),(9,3),(9,15)]
starPoints 9  = [(6, 2), (2, 6), (6, 6), (2, 2), (4, 4), (4, 2), (2, 4), (4, 6), (6, 4)]
starPoints _  = []

currMove :: Board -> Stone
currMove Board {moves = []} = Black
currMove board = toStone $ (length . moves $ board) - (handleHandicap . handicap . options $ board)
    where
        toStone l
            | l `mod` 2 = Black
            | otherwise = White

        handleHandicap x
            | x > 2 = x
            | otherwise = 0

inBounds :: Board -> (Int, Int) -> Bool
inBounds b = inRange (A.bounds . stones $ b)


fromMaybeFn :: (a -> Maybe a) -> a -> (a, Bool)
fromMaybeFn fn x = (fromMaybe x app, isJust app)
    where
        app = fn x


makeMove' loc board = fst <$> makeMove loc board

-- makes the move on the board
-- Nothing if illegal
-- Just (Board, stonesEaten) if not
makeMove :: (Int, Int) -> Board -> Maybe (Board, Bool)
makeMove loc board
    | illegal = Nothing
    | otherwise = Just . fromMaybeFn (applyEats loc) . placeStone loc side $ board
    where
        illegal = not . inBounds board $ loc -- && (surrounded loc board /= Nothing)
        side = stoneNot . fromMaybe White $ (flip getStone) board =<< (listToMaybe . moves $ board)


getStone :: (Int, Int) -> Board -> Maybe Stone
getStone p (Board {stones = stones}) = stones A.! p

lineConcat :: String -> String -> String
lineConcat a b = a ++ '\n' : b

allStones :: Board -> [[(Int, Int)]]
allStones b = map (\x -> map ((,) x) line) line
    where
        line = [0 .. size . options $ b]

placeStone :: (Int, Int) -> Stone -> Board -> Board
placeStone loc stone (Board {stones = stones,
                             moves = moves,
                             options = options})
                        = Board {stones = stones A.// [(loc, Just stone)],
                                 moves  = loc : moves,
                                 options = options}


-- Eats surrounding stones
applyEats :: (Int, Int) -> Board -> Maybe Board
applyEats p board = removeStones surroundingHostileEatenStones
        where

            removeStones :: Set.Set (Int, Int) -> Maybe Board
            removeStones s
                | Set.null s    = Nothing
                | otherwise = Just $ board {stones = (stones board) A.// (zip (Set.toList s) (repeat Nothing))}

            currStone :: Maybe Stone
            currStone = getStone p board

            surroundingHostileEatenStones :: Set.Set (Int, Int)
            surroundingHostileEatenStones =
                  Set.unions
                . catMaybes
                . map ((flip surrounded) board)
                . filter isHostile
                . neighbors board $ p

            isHostile :: (Int, Int) -> Bool
            isHostile candidate = (getStone candidate board) == (stoneNot <$> currStone)


neighbors :: Board -> (Int, Int) -> [(Int, Int)]
neighbors b (x, y) = filter (inBounds b)
                  [(x + 1, y),
                   (x - 1, y),
                   (x, y + 1),
                   (x, y - 1)]


-- returns nothing if not surrounded
-- otherwise, returns a blob of the surrounded stones
surrounded :: (Int, Int) -> Board -> Maybe (Set.Set (Int, Int))
surrounded start board = blob start Set.empty
    where

        side :: Maybe Stone
        side = getStone start board

        blob :: (Int, Int) -> Set.Set (Int, Int) -> Maybe (Set.Set (Int, Int))
        blob p accum
          -- is blank space (not eaten)
          | Nothing == currStone = Nothing
          -- seen this spot already
          | p `Set.member` accum = Just Set.empty
          -- spot is my stone's color
          | side == currStone = foldl (liftA2 Set.union) (Just inserted) . map (flip blob inserted) . neighbors board $ p
          | otherwise = Just Set.empty
            where
                currStone = getStone p board
                inserted = Set.insert p accum
